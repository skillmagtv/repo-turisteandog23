package com.backend.turisteandog23.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
@Table(name = "departamento")
public class Departamento implements Serializable{
    @Id
    @Column(name = "id_depto")
    private Integer id_depto;
    @NotEmpty(message = "El campo descripcion de Departamento no debe ser vacio")
    @Size(min = 2, max = 45, message = "El campo descripcion de departamento debe tener minimo 2 caracteres y máximo 45")
    @Column(name = "descripcion")
    private String  descripcion;

    
    
}