package com.backend.turisteandog23.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Setter;
import lombok.Getter;

@Getter
@Setter
@Entity
@Table(name = "ciudad")
public class Ciudad implements Serializable {
    @Id
    @Column(name = "id_ciudad")
    private int id_ciudad;
    @ManyToOne
    @JoinColumn(name = "id_depto")
    private Departamento departamento;
    //@NotEmpty(message = "El campo Descripcion no debe ser vacio")
    //@Size(min = 2, max = 45, message = "El campo descripcion de departamento debe tener minimo 2 caracteres y máximo 45")
    @Column(name = "descripcion")
    private String descripcion;
    
    @Override
    public String toString() {
        return "Ciudad [id_ciudad=" + id_ciudad + ", departamento=" + departamento + ", descripcion=" + descripcion + "]";
    }   
}