package com.backend.turisteandog23.Models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="experiencia")
public class Experiencia implements Serializable {
    @Id
    @Column(name = "id_experiencia")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id_experiencia;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;
    @ManyToOne
    @JoinColumn(name = "id_atraccion")
    private Atraccion atraccion;
    @Column(name = "fecha")
    private String fecha;
    @Column(name = "descripcion")
    private String descripcion;
}
