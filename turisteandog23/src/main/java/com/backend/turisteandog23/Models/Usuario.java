package com.backend.turisteandog23.Models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//Para validar los errores
import javax.validation.constraints.NotEmpty;
//import javax.validation.constraints.Size;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Data
@Table(name="usuario")
public class Usuario implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private int id_usuario;
    @NotEmpty(message = "El campo de Nickname no debe ser vacío")
    @Column(name = "nickname") 
    private String nickname;
    @NotEmpty(message = "El campo de nombre no debe ser vacío")
    @Column(name = "nombre")
    private String nombre;
    @NotEmpty(message = "El campo de apellido no debe ser vacío")
    @Column(name = "apellido")
    private String apellido;
    @ManyToOne
    @JoinColumn(name = "tipo_documento")
    private Tipodocumento tipo_documento;
    @NotEmpty(message = "El campo de contraseña no debe ser vacío")
    @Column(name = "contrasena")
    private String contrasena;
    @ManyToOne
    @JoinColumn(name = "tipo_usuario")
    private Tipousuario tipo_usuario;
    @NotEmpty(message = "El campo de correo no debe ser vacío")
    @Column(name = "correo")
    private String correo;
    @NotEmpty(message = "El campo de celular no debe ser vacío")
    @Column(name = "celular")
    private String celular;
    @NotEmpty(message = "El campo de pais_origen no debe ser vacío")
    @Column(name = "pais_origen")
    private String pais_origen;
    @NotEmpty(message = "El campo de ciudad no debe ser vacío")
    @Column(name = "ciudad")
    private String ciudad;
    @NotEmpty(message = "El campo de dirección no debe ser vacío")
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "rnt")
    private int rnt;
    @NotEmpty(message = "El campo de idioma no debe ser vacío")
    @Column(name = "idioma")
    private String idioma;

    @Override
    public String toString() {
        return "Usuario [id_usuario=" + id_usuario + ", nickname=" + nickname + ", nombre=" + nombre + ", apellido="
                + apellido + ", tipo_documento=" + tipo_documento + ", contrasena=" + contrasena + ", tipo_usuario="
                + tipo_usuario + ", correo=" + correo + ", celular=" + celular + ", pais_origen=" + pais_origen
                + ", ciudad=" + ciudad + ", direccion=" + direccion + ", rnt=" + rnt + ", idioma=" + idioma + "]";
    }
} 
    
   

    

    

