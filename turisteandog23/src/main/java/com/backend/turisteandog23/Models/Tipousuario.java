package com.backend.turisteandog23.Models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(name = "tipousuario")
public class Tipousuario implements Serializable{
    @Id
    @Column(name="id_tipo_usuario")
    private Integer id_tipo_usuario;
    @Column(name="descripcion")
    private String descripcion;
    
    @Override
    public String toString() {
        return "Tipousuario [id_tipo_usuario=" + id_tipo_usuario +  ", descripcion=" + descripcion + "]";
    }
    
}
