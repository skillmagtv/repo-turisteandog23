package com.backend.turisteandog23.Models;

import lombok.Data;

@Data
public class Error {
    private String field;
    private String message;
}
