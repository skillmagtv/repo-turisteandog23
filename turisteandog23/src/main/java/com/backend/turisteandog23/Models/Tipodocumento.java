package com.backend.turisteandog23.Models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(name="tipodocumento")
public class Tipodocumento implements Serializable{
    @Id
    @Column(name="tipo_documento")
    private Integer tipo_documento;
    @Column(name="abreviatura")
    private String abreviatura;
    @Column(name="descripcion")
    private String descripcion;
    
    @Override
    public String toString() {
        return "Tipodocumento [tipo_documento=" + tipo_documento + ", abreviatura=" + abreviatura + ", descripcion="
                + descripcion + "]";
    }
    
}
