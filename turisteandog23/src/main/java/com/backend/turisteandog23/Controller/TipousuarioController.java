package com.backend.turisteandog23.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.backend.turisteandog23.Dao.UsuarioDao;
import com.backend.turisteandog23.Models.Tipousuario;
import com.backend.turisteandog23.Models.Usuario;
import com.backend.turisteandog23.Security.Hash;
import com.backend.turisteandog23.Service.TipousuarioService;


@RestController
@CrossOrigin("*")
@RequestMapping("/tipousuario")
public class TipousuarioController {
    @Autowired
    private TipousuarioService tipousuarioService;
    @Autowired
    private UsuarioDao usuarioDao;

    @PostMapping(value="/")
    @ResponseBody
    public ResponseEntity<Tipousuario> agregar(@RequestHeader("clave")String clave,@RequestHeader ("cliente") Integer cliente,@Valid @RequestBody Tipousuario tipousuario){
        Usuario objeto=new Usuario();
        objeto=usuarioDao.login(cliente, Hash.sha1(clave));
        if (objeto!=null) {
            return new ResponseEntity<>(tipousuarioService.save(tipousuario), HttpStatus.OK); 
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); 
        }
    }

    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Tipousuario> eliminar(@PathVariable Integer id){ 
        Tipousuario obj = tipousuarioService.findById(id); 
            if(obj!=null) 
                tipousuarioService.delete(id);
            else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
            return new ResponseEntity<>(obj, HttpStatus.OK);  
    }

    @PutMapping(value="/") 
    @ResponseBody
    public ResponseEntity<Tipousuario> editar(@Valid @RequestBody Tipousuario tipousuario){ 
        Tipousuario obj = tipousuarioService.findById(tipousuario.getId_tipo_usuario()); 
        if(obj!=null) { 
            obj.setDescripcion(tipousuario.getDescripcion());
            tipousuarioService.save(tipousuario);
        } 
        else 
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }

    @GetMapping("/list") 
    @ResponseBody
    public List<Tipousuario> consultarTodo(@RequestHeader("clave")String clave,@RequestHeader ("cliente") Integer cliente){
        return tipousuarioService.findAll();
    }

    @GetMapping("/list/{id}") 
    @ResponseBody
    public Tipousuario consultaPorId(@PathVariable Integer id){ 
        return tipousuarioService.findById(id); 
    }
    
    @GetMapping("/consulta_tipousuario")
    @ResponseBody
    public ResponseEntity<List<Tipousuario>> consulta_tipousuario(@RequestParam ("idc") String idc,@RequestHeader ("cliente") Integer cliente,@RequestHeader ("clave") String clave) { 
        Usuario objeto=new Usuario();
        objeto=usuarioDao.login(cliente, Hash.sha1(clave));
        if (objeto!=null) {
            return new ResponseEntity<>(tipousuarioService.consulta_tipousuario(idc),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    } 
}
