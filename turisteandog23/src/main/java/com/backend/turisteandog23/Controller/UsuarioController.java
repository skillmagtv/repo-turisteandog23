package com.backend.turisteandog23.Controller;

import com.backend.turisteandog23.Models.Usuario;
import com.backend.turisteandog23.Security.Hash;
import com.backend.turisteandog23.Service.UsuarioService;
import com.backend.turisteandog23.Dao.UsuarioDao;
import java.util.List;
import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;




@RestController
@CrossOrigin("*")
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioDao dao; 
    @Autowired
    private UsuarioService servicio;

    @PostMapping(value="/")
    @ResponseBody
    public ResponseEntity<Usuario> agregar(@RequestHeader("clave")String clave,@RequestHeader("cliente")Integer cliente,@Valid @RequestBody Usuario dato){   
        Usuario objeto=new Usuario();
        objeto=dao.login(cliente, Hash.sha1(clave));
        if (objeto!=null) {
            dato.setContrasena(Hash.sha1(dato.getContrasena()));
            return new ResponseEntity<>(servicio.save(dato), HttpStatus.OK); 
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); 
        }
            
    }
    //Metodo delete para eliminar datos
    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Usuario> eliminar(@PathVariable Integer id,@RequestHeader("clave")String clave,@RequestHeader("cliente")Integer cliente){ 
        Usuario objeto=new Usuario();
        objeto=dao.login(cliente, Hash.sha1(clave));
        if (objeto!=null) {
            Usuario obj = servicio.findById(id); 
            if(obj!=null) 
            servicio.delete(id);
            else 
                return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
            return new ResponseEntity<>(obj, HttpStatus.OK); 
      
       } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
       }
       
        
    }
    //Metodo put para Modificar Datos
    @PutMapping(value="/") 
    @ResponseBody
    public ResponseEntity<Usuario> editar(@RequestHeader("clave")String clave,@RequestHeader("cliente")Integer cliente,@Valid @RequestBody Usuario dato){ 
        Usuario objeto=new Usuario();
        objeto=dao.login(cliente, Hash.sha1(clave));
        if(objeto!=null) { 
            dato.setContrasena(Hash.sha1(dato.getContrasena()));
            Usuario obj = servicio.findById(dato.getId_usuario()); 
            if(obj!=null) { 
            obj.setNickname(dato.getNickname());
            obj.setNombre(dato.getNombre());
            obj.setApellido(dato.getApellido());
            obj.setTipo_documento(dato.getTipo_documento());
            obj.setContrasena(dato.getContrasena());
            obj.setTipo_usuario(dato.getTipo_usuario());
            obj.setCorreo(dato.getCorreo());
            obj.setCelular(dato.getCelular());
            obj.setPais_origen(dato.getPais_origen());
            obj.setCiudad(dato.getCiudad());
            obj.setDireccion(dato.getDireccion());
            obj.setRnt(dato.getRnt());
            obj.setIdioma(dato.getIdioma());
            servicio.save(dato); 
        } 
                else 
                     return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
                return new ResponseEntity<>(obj, HttpStatus.OK); 
            } else {
                 return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        }   
    


    //Metodo list para obtener la lista de la tabla
    
    @GetMapping("/list") 
    @ResponseBody
    public ResponseEntity<List<Usuario>> consultarTodo(@RequestHeader("clave")String clave,@RequestHeader("cliente")Integer cliente){
        Usuario objeto=new Usuario();
        objeto=dao.login(cliente, Hash.sha1(clave));
        if (objeto!=null) {
            return new ResponseEntity<>(servicio.findAll(),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }  
    }
    

    @GetMapping("/list/{id}") 
    @ResponseBody
    public ResponseEntity<Usuario> consultaPorId(@PathVariable Integer id,@RequestHeader("clave")String clave,@RequestHeader("cliente")Integer cliente){ 
        Usuario objeto=new Usuario();
        objeto=dao.login(cliente, Hash.sha1(clave));
        if (objeto!=null) {
            return new ResponseEntity<>(servicio.findById(id),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }   
    }
    
    @GetMapping("/login")
    @ResponseBody
    public Usuario ingresar(@RequestParam ("cliente") Integer cliente,@RequestParam ("clave") String clave) {
        clave=Hash.sha1(clave);
        return servicio.login(cliente, clave); 
    }
}

    
