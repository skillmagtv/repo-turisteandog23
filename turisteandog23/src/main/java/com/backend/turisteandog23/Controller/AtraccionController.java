package com.backend.turisteandog23.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.backend.turisteandog23.Dao.UsuarioDao;
import com.backend.turisteandog23.Models.Atraccion;
import com.backend.turisteandog23.Models.Usuario;
import com.backend.turisteandog23.Security.Hash;
import com.backend.turisteandog23.Service.AtraccionService;

@RestController
@CrossOrigin("*")
@RequestMapping("/atraccion")
public class AtraccionController {
    @Autowired
    private AtraccionService atraccionService;
    @Autowired
    private UsuarioDao usuarioDao;

    @PostMapping(value="/")
    @ResponseBody
    public ResponseEntity<Atraccion> agregar(@RequestHeader("clave")String clave,@RequestHeader ("cliente") Integer cliente,@Valid @RequestBody Atraccion atraccion){
        Usuario objeto=new Usuario();
        objeto=usuarioDao.login(cliente, Hash.sha1(clave));
        if (objeto!=null) {
            return new ResponseEntity<>(atraccionService.save(atraccion), HttpStatus.OK); 
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); 
        }
    }

    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Atraccion> eliminar(@PathVariable Integer id){ 
        Atraccion obj = atraccionService.findById(id); 
            if(obj!=null) 
                atraccionService.delete(id);
            else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
            return new ResponseEntity<>(obj, HttpStatus.OK);  
    }

    @PutMapping(value="/") 
    @ResponseBody
    public ResponseEntity<Atraccion> editar(@RequestBody Atraccion atraccion){ 
        Atraccion obj = atraccionService.findById(atraccion.getId_atraccion()); 
        if(obj!=null) { 
            obj.setCiudad(atraccion.getCiudad());
            obj.setDescripcion(atraccion.getDescripcion());
            atraccionService.save(atraccion); 
        } 
        else 
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }

    @GetMapping("/list") 
    @ResponseBody
    public List<Atraccion> consultarTodo(@RequestHeader("clave")String clave,@RequestHeader ("cliente") Integer cliente){
        return atraccionService.findAll();
    }

    @GetMapping("/list/{id}") 
    @ResponseBody
    public Atraccion consultaPorId(@PathVariable Integer id){ 
        return atraccionService.findById(id); 
    }
    
    @GetMapping("/consulta_atraccion")
    @ResponseBody
    public ResponseEntity<List<Atraccion>> consulta_atraccion(@RequestParam ("idc") String idc,@RequestHeader ("cliente") Integer cliente,@RequestHeader ("clave") String clave) { 
        Usuario objeto=new Usuario();
        objeto=usuarioDao.login(cliente, Hash.sha1(clave));
        if (objeto!=null) {
            return new ResponseEntity<>(atraccionService.consulta_atraccion(idc),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        
    }
}
