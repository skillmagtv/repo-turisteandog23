package com.backend.turisteandog23.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//import backend.demo.Dao.CiudadDao;
import com.backend.turisteandog23.Models.Ciudad;
import com.backend.turisteandog23.Service.CiudadService;

@RestController
@CrossOrigin("*")
@RequestMapping("/ciudad")

public class CiudadController {
    //@Autowired
    //private DepartamentoDao departamentoDao;
    @Autowired
    private CiudadService ciudadService;

    @PostMapping(value="/")
    @ResponseBody
    public ResponseEntity<Ciudad> agregar(@Valid @RequestBody Ciudad ciudad){   
        Ciudad obj = ciudadService.save(ciudad);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Ciudad> eliminar(@PathVariable Integer id){ 
            Ciudad obj = ciudadService.findById(id); 
            if(obj!=null) 
                ciudadService.delete(id);
            else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
            return new ResponseEntity<>(obj, HttpStatus.OK);  
    }

    @PutMapping(value="/") 
    @ResponseBody
    public ResponseEntity<Ciudad> editar(@Valid @RequestBody Ciudad ciudad){ 
        Ciudad obj = ciudadService.findById(ciudad.getId_ciudad()); 
        if(obj!=null) { 
            obj.setDepartamento(ciudad.getDepartamento());
            obj.setDescripcion(ciudad.getDescripcion());
            ciudadService.save(ciudad); 
        } 
        else 
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }

    @GetMapping("/list") 
    @ResponseBody
    public List<Ciudad> consultarTodo(){
        return ciudadService.findAll();
    }

    @GetMapping("/list/{id}") 
    @ResponseBody
    public Ciudad consultaPorId(@PathVariable Integer id){ 
        return ciudadService.findById(id); 
    }   
}
