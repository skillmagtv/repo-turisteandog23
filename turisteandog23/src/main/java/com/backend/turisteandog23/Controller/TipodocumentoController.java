package com.backend.turisteandog23.Controller;

import com.backend.turisteandog23.Dao.UsuarioDao;
import com.backend.turisteandog23.Models.Tipodocumento;
import com.backend.turisteandog23.Models.Usuario;
import com.backend.turisteandog23.Security.Hash;
import com.backend.turisteandog23.Service.TipodocumentoService;
//import com.backend.turisteandog23.Dao.TipodocumentoDao;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/tipodocumento")
public class TipodocumentoController {

    
    //@Autowired
   // private TipodocumentoDao tipodocumentoDao; 
    @Autowired
    private TipodocumentoService tipodocumentoService;
    @Autowired
    private UsuarioDao usuarioDao;

    //Metodo Post para Insertar datos en la BD
    @PostMapping(value="/")
    @ResponseBody
    public ResponseEntity<Tipodocumento> agregar(@RequestHeader("clave")String clave,@RequestHeader ("cliente") Integer cliente,@Valid @RequestBody Tipodocumento tipodocumento){
        Usuario objeto=new Usuario();
        objeto=usuarioDao.login(cliente, Hash.sha1(clave));
        if (objeto!=null) {
            return new ResponseEntity<>(tipodocumentoService.save(tipodocumento), HttpStatus.OK); 
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); 
        }
    }

    //Metodo delete para eliminar datos
    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Tipodocumento> eliminar(@PathVariable Integer id){

        Tipodocumento obj = tipodocumentoService.findById(id); 
            if(obj!=null) //Encontro el registro
                tipodocumentoService.delete(id);
            else 
                return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
            return new ResponseEntity<>(obj, HttpStatus.OK);  
    }

    //Metodo put para Modificar Datos
    @PutMapping(value="/") 
    @ResponseBody
    public ResponseEntity<Tipodocumento> editar(@RequestBody Tipodocumento tipodocumento){ 
        Tipodocumento obj = tipodocumentoService.findById(tipodocumento.getTipo_documento()); 
        if(obj!=null) { 
            obj.setAbreviatura(tipodocumento.getAbreviatura());
            obj.setDescripcion(tipodocumento.getDescripcion());
            tipodocumentoService.save(tipodocumento); 
        } 
        else 
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }

    //Metodo list para obtener la lista de la tabla
    @GetMapping("/list") 
    //@ResponseBody
    public List<Tipodocumento> consultarTodo(){
        return tipodocumentoService.findAll();
    }
    @GetMapping("/list/{id}") 
    
    @ResponseBody
    public Tipodocumento consultaPorId(@PathVariable Integer id){ 
        return tipodocumentoService.findById(id); 
    }
    
    
}
