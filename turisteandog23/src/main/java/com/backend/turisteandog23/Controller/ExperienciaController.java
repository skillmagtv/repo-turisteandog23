package com.backend.turisteandog23.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.backend.turisteandog23.Dao.UsuarioDao;
import com.backend.turisteandog23.Models.Experiencia;
import com.backend.turisteandog23.Models.Usuario;
import com.backend.turisteandog23.Security.Hash;
import com.backend.turisteandog23.Service.ExperienciaService;

@RestController
@CrossOrigin("*")
@RequestMapping("/experiencia")
public class ExperienciaController {
    @Autowired
    private ExperienciaService experienciaService;
    @Autowired
    private UsuarioDao usuarioDao;

    @PostMapping(value="/")
    @ResponseBody
    public ResponseEntity<Experiencia> agregar(@RequestHeader("clave")String clave,@RequestHeader ("cliente") Integer cliente,@Valid @RequestBody Experiencia experiencia){
        Usuario objeto=new Usuario();
        objeto=usuarioDao.login(cliente, Hash.sha1(clave));
        if (objeto!=null) {
            return new ResponseEntity<>(experienciaService.save(experiencia), HttpStatus.OK); 
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); 
        }
    }

    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Experiencia> eliminar(@PathVariable Integer id){ 
        Experiencia obj = experienciaService.findById(id); 
            if(obj!=null) 
                experienciaService.delete(id);
            else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
            return new ResponseEntity<>(obj, HttpStatus.OK);  
    }

    @PutMapping(value="/") 
    @ResponseBody
    public ResponseEntity<Experiencia> editar( @RequestBody Experiencia experiencia){ 
        Experiencia obj = experienciaService.findById(experiencia.getId_experiencia()); 
        if(obj!=null) { 
            obj.setUsuario(experiencia.getUsuario());
            obj.setAtraccion(experiencia.getAtraccion());
            obj.setFecha(experiencia.getFecha());
            obj.setDescripcion(experiencia.getDescripcion());
            experienciaService.save(experiencia); 
        } 
        else 
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
        return new ResponseEntity<>(obj, HttpStatus.OK); 
    }

    @GetMapping("/list") 
    @ResponseBody
    public List<Experiencia> consultarTodo(@RequestHeader("clave")String clave,@RequestHeader ("cliente") Integer cliente){
        return experienciaService.findAll();
    }

    @GetMapping("/list/{id}") 
    @ResponseBody
    public Experiencia consultaPorId(@PathVariable Integer id){ 
        return experienciaService.findById(id); 
    }

    @GetMapping("/consulta_experiencia")
    @ResponseBody
    public ResponseEntity<List<Experiencia>> consulta_atraccion(@RequestParam ("idc") String idc,@RequestHeader ("cliente") Integer cliente,@RequestHeader ("clave") String clave) { 
        Usuario objeto=new Usuario();
        objeto=usuarioDao.login(cliente, Hash.sha1(clave));
        if (objeto!=null) {
            return new ResponseEntity<>(experienciaService.consulta_experiencia(idc),HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        
    }
}
