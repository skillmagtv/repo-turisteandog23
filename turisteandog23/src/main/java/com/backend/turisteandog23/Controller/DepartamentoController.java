package com.backend.turisteandog23.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.backend.turisteandog23.Dao.UsuarioDao;
//import backend.demo.Dao.DepartamentoDao;
import com.backend.turisteandog23.Models.Departamento;
import com.backend.turisteandog23.Models.Usuario;
import com.backend.turisteandog23.Security.Hash;
import com.backend.turisteandog23.Service.DepartamentoService;

@RestController
@CrossOrigin("*")
@RequestMapping("/departamento")
public class DepartamentoController {
    @Autowired
    private DepartamentoService departamentoService;
    @Autowired
    private UsuarioDao usuarioDao;

    @PostMapping(value="/")
    @ResponseBody
    public ResponseEntity<Departamento> agregar(@Valid @RequestBody Departamento departamento, @RequestHeader("clave")String clave,@RequestHeader ("cliente") Integer cliente){   
        Usuario objeto=new Usuario();
        objeto=usuarioDao.login(cliente, Hash.sha1(clave));
        if (objeto!=null) {
            return new ResponseEntity<>(departamentoService.save(departamento), HttpStatus.OK); 
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED); 
        }
    }

    @DeleteMapping(value="/{id}") 
    public ResponseEntity<Departamento> eliminar(@PathVariable Integer id){ 
            Departamento obj = departamentoService.findById(id); 
            if(obj!=null) 
                departamentoService.delete(id);
            else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
            return new ResponseEntity<>(obj, HttpStatus.OK);  
    }

    @PutMapping(value="/") 
    @ResponseBody
    public ResponseEntity<Departamento> editar(@Valid @RequestBody Departamento departamento){ 
        Departamento obj = departamentoService.findById(departamento.getId_depto()); 
        if(obj!=null) { 
            obj.setDescripcion(departamento.getDescripcion());
            departamentoService.save(departamento); 
        } 
        else 
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR); 
            return new ResponseEntity<>(obj, HttpStatus.OK); 
    }

    @GetMapping("/list") 
    @ResponseBody
    public List<Departamento> consultarTodo(@RequestHeader("clave")String clave,@RequestHeader ("cliente") Integer cliente){
        return departamentoService.findAll();
    }

    @GetMapping("/list/{id}") 
    @ResponseBody
    public Departamento consultaPorId(@PathVariable Integer id){ 
        return departamentoService.findById(id); 
    }
}
