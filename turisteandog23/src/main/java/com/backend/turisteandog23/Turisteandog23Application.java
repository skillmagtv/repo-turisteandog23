package com.backend.turisteandog23;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Turisteandog23Application {

	public static void main(String[] args) {
		SpringApplication.run(Turisteandog23Application.class, args);
	}

}
