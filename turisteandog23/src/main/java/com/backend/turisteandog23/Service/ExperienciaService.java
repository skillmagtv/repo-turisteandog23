package com.backend.turisteandog23.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.turisteandog23.Dao.ExperienciaDao;
import com.backend.turisteandog23.Models.Experiencia;

@Service
public class ExperienciaService {
    @Autowired
    private ExperienciaDao experienciaDao;

    public Experiencia save(Experiencia experiencia){
        return experienciaDao.save(experiencia);
    }
    
    @Transactional(readOnly = false)
    public void delete(Integer id){
        experienciaDao.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Experiencia findById(Integer id){
        return experienciaDao.findById(id).orElse(null);
    }

    public List<Experiencia> findAll(){
        return (List<Experiencia>) experienciaDao.findAll();
    }
    @Transactional(readOnly=true)
    public List<Experiencia> consulta_experiencia(String idc) {
        return (List<Experiencia>) experienciaDao.consulta_experiencia(idc);
    }
}
