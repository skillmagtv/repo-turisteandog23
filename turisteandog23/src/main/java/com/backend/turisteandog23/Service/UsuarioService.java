package com.backend.turisteandog23.Service;

import com.backend.turisteandog23.Models.Usuario;
import com.backend.turisteandog23.Dao.UsuarioDao;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioDao dao;

    public Usuario save(Usuario objeto){
        return dao.save(objeto);

    }

    @Transactional(readOnly = false)
    public void delete(Integer id){
        dao.deleteById(id);

    }

    @Transactional(readOnly = true)
    public Usuario findById(Integer id){
        return dao.findById(id).orElse(null);
    }

    @Transactional(readOnly=true)
    public List<Usuario> findAll(){
        return (List<Usuario>) dao.findAll();
    }

    
    @Transactional(readOnly=true)
    public Usuario login(Integer usuario, String clave) {
        return dao.login(usuario, clave);
    }

}
