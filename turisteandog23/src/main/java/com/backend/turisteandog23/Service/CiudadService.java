package com.backend.turisteandog23.Service;

import com.backend.turisteandog23.Models.Ciudad;
import com.backend.turisteandog23.Dao.CiudadDao;
import java.util.List;

//import javax.print.attribute.standard.MediaSize.Other;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CiudadService {
    @Autowired
    private CiudadDao ciudadDao;

    public Ciudad save(Ciudad Ciudad){
        return ciudadDao.save(Ciudad);
    }
    
    @Transactional(readOnly = false)
    public void delete(Integer id){
        ciudadDao.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Ciudad findById(Integer id){
        return ciudadDao.findById(id).orElse(null);
    }

    public List<Ciudad> findAll(){
        return (List<Ciudad>) ciudadDao.findAll();
    }
}
