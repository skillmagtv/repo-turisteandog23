package com.backend.turisteandog23.Service;

import com.backend.turisteandog23.Models.Departamento;
import com.backend.turisteandog23.Dao.DepartamentoDao;
import java.util.List;

//import javax.print.attribute.standard.MediaSize.Other;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DepartamentoService {
    @Autowired
    private DepartamentoDao departamentoDao;

    public Departamento save(Departamento departamento){
        return departamentoDao.save(departamento);
    }

    @Transactional(readOnly = false)
    public void delete(Integer id){
        departamentoDao.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Departamento findById(Integer id){
        return departamentoDao.findById(id).orElse(null);
    }

    public List<Departamento> findAll(){
        return (List<Departamento>) departamentoDao.findAll();
    }
    
}
