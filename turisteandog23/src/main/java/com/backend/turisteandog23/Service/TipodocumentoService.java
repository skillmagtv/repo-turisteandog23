package com.backend.turisteandog23.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.turisteandog23.Dao.TipodocumentoDao;
import com.backend.turisteandog23.Models.Tipodocumento;

@Service
public class TipodocumentoService {

    @Autowired
    private TipodocumentoDao tipodocumentoDao;

    public Tipodocumento save(Tipodocumento tipodocumento){
        return tipodocumentoDao.save(tipodocumento);
    }

    @Transactional(readOnly = false)
    public void delete(Integer id){
        tipodocumentoDao.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Tipodocumento findById(Integer id){
        return tipodocumentoDao.findById(id).orElse(null);
    }

    @Transactional(readOnly =true)
    public List<Tipodocumento> findAll(){
        return (List<Tipodocumento>) tipodocumentoDao.findAll();
    }
}
