package com.backend.turisteandog23.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.turisteandog23.Dao.AtraccionDao;
import com.backend.turisteandog23.Models.Atraccion;

@Service
public class AtraccionService {
    @Autowired
    private AtraccionDao atraccionDao;

    @Transactional(readOnly = false)
    public Atraccion save(Atraccion atraccion){
        return atraccionDao.save(atraccion);
    }
    
    @Transactional(readOnly = false)
    public void delete(Integer id){
        atraccionDao.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Atraccion findById(Integer id){
        return atraccionDao.findById(id).orElse(null);
    }

    public List<Atraccion> findAll(){
        return (List<Atraccion>) atraccionDao.findAll();
    }
    @Transactional(readOnly=true)
    public List<Atraccion> consulta_atraccion(String idc) {
        return (List<Atraccion>) atraccionDao.consulta_atraccion(idc);
    }
}
