package com.backend.turisteandog23.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.turisteandog23.Dao.TipousuarioDao;
import com.backend.turisteandog23.Models.Tipousuario;

@Service
public class TipousuarioService {
    @Autowired
    private TipousuarioDao tipousuarioDao;

    public Tipousuario save(Tipousuario Tipousuario){
        return tipousuarioDao.save(Tipousuario);
    }
    
    @Transactional(readOnly = false)
    public void delete(Integer id){
        tipousuarioDao.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Tipousuario findById(Integer id){
        return tipousuarioDao.findById(id).orElse(null);
    }

    public List<Tipousuario> findAll(){
        return (List<Tipousuario>) tipousuarioDao.findAll();
    }
    @Transactional(readOnly=true)
    public List<Tipousuario> consulta_tipousuario(String idc) {
        return (List<Tipousuario>) tipousuarioDao.consulta_tipousuario(idc);
    }
}