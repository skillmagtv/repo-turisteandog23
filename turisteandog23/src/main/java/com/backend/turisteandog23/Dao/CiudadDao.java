package com.backend.turisteandog23.Dao;

import com.backend.turisteandog23.Models.Ciudad;

import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
//import org.springframework.data.repository.query.Param;
//import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Transactional;

public interface CiudadDao extends CrudRepository<Ciudad,Integer>{
    @Transactional(readOnly=true)//No afecta integridad base de datos
    @Query(value="SELECT * FROM ciudad WHERE id_usuario= :usuario AND contrasena= :clave", nativeQuery=true)
    public Ciudad login(@Param("usuario") Integer usuario, @Param("clave") String clave); 
}
