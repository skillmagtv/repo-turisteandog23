package com.backend.turisteandog23.Dao;

import com.backend.turisteandog23.Models.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UsuarioDao extends CrudRepository<Usuario, Integer>{
    //Operación de Autentiiicación (SELECT)
    @Transactional(readOnly=true)//No afecta integridad base de datos
    @Query(value="SELECT * FROM usuario WHERE id_usuario= :cliente AND contrasena= :clave", nativeQuery=true)
    public Usuario login(@Param("cliente") Integer cliente, @Param("clave") String clave); 

}
