package com.backend.turisteandog23.Dao;

import com.backend.turisteandog23.Models.Atraccion;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
//import org.springframework.data.repository.query.Param;
//import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Transactional;

public interface AtraccionDao extends CrudRepository<Atraccion,Integer> {
    //Operación para seleccionar cuentas de un cliente en particular (SELECT)
   @Transactional(readOnly=true)//No afecta integridad base de datos
   @Query(value="SELECT * FROM usuario WHERE id_usuario= :idc", nativeQuery=true)
   public List<Atraccion> consulta_atraccion(@Param("idc") String idc); 
}
