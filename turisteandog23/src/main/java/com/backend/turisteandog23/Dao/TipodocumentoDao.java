package com.backend.turisteandog23.Dao;

import com.backend.turisteandog23.Models.Tipodocumento;

//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.repository.query.Param;
//import org.springframework.transaction.annotation.Transactional;

public interface TipodocumentoDao extends CrudRepository<Tipodocumento,Integer>{
    
}
