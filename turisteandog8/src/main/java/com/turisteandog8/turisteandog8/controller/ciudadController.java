/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.turisteandog8.turisteandog8.controller;

import com.turisteandog8.turisteandog8.modelo.ciudad;
import com.turisteandog8.turisteandog8.services.ciudadService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author David
 */

@CrossOrigin(origins = {"http://localhost:4406", "*"})
@RestController
@RequestMapping("/ciudad")


public class ciudadController {
    
    @Autowired
    private ciudadService ciudadService;
    
    @PostMapping(value = "/")
    public ResponseEntity<ciudad> agregar(@RequestBody ciudad ciudad) {
        ciudad obj = ciudadService.save(ciudad);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ciudad> eliminar(@PathVariable Integer id) {
        ciudad obj = ciudadService.findById(id);
        if (obj != null) {
        ciudadService.delete(id);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @PutMapping(value = "/")
    public ResponseEntity<ciudad> editar(@RequestBody ciudad ciudad) {
        ciudad obj = ciudadService.findById(ciudad.getIdCiudad());
        if (obj != null) {
        obj.setDescripcion(ciudad.getDescripcion());
        ciudadService.save(obj);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @GetMapping("/list")
    public List<ciudad> consultarTodo() {
        return ciudadService.findAll();
    }
    @GetMapping("/list/{id}")
    public ciudad consultaPorId(@PathVariable Integer id) {
        return ciudadService.findById(id);
    }
    
}
