/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.controller;

import com.turisteandog8.turisteandog8.modelo.experiencia;
import com.turisteandog8.turisteandog8.services.experienciaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author nicol
 */
@CrossOrigin(origins = {"http://localhost:4200", "*"})
@RestController
@RequestMapping("/experiencia")
public class experienciaController {
    @Autowired
    private experienciaService experienciaService;
    
    @PostMapping(value = "/")
    public ResponseEntity<experiencia>agregar(@RequestBody experiencia experiencia){
        experiencia obj = experienciaService.save(experiencia);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<experiencia>eliminar(@PathVariable Integer id){
        experiencia obj =  experienciaService.findById(id);
        if(obj !=null){
            experienciaService.delete(id);
        }
        else {
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @PutMapping(value = "/")
    public ResponseEntity<experiencia> editar(@RequestBody experiencia experiencia) {
        experiencia obj = experienciaService.findById(experiencia.getIdExperiencia());
        if (obj != null) {
        obj.setTipoUsuario(experiencia.getTipoUsuario());
        obj.setFecha(experiencia.getFecha());
        obj.setDescripcion(experiencia.getDescripcion());
        experienciaService.save(obj);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @GetMapping("/list")
    public List<experiencia> consultarTodo() {
        return experienciaService.findAll();
    }
    @GetMapping("/list/{id}")
    public experiencia consultaPorId(@PathVariable Integer id) {
        return experienciaService.findById(id);
    }
}
