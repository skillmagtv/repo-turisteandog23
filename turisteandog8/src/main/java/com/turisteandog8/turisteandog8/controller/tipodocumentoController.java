/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.controller;

import com.turisteandog8.turisteandog8.modelo.tipodocumento;
import com.turisteandog8.turisteandog8.services.tipodocumentoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author mateo
 */
@CrossOrigin(origins = {"http://localhost:4200", "*"})
@RestController
@RequestMapping("/tipodocumento")
public class tipodocumentoController {
    @Autowired
    private tipodocumentoService tipodocumentoService;
    
    @PostMapping(value = "/")
    public ResponseEntity<tipodocumento>agregar(@RequestBody tipodocumento tipodocumento){
        tipodocumento obj = tipodocumentoService.save(tipodocumento);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<tipodocumento>eliminar(@PathVariable Integer id){
        tipodocumento obj =  tipodocumentoService.findById(id);
        if(obj !=null){
            tipodocumentoService.delete(id);
        }
        else {
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @PutMapping(value = "/")
    public ResponseEntity<tipodocumento> editar(@RequestBody tipodocumento tipodocumento) {
        tipodocumento obj = tipodocumentoService.findById(tipodocumento.getIdTipodocumento());
        if (obj != null) {
        obj.setTipoDocumento(tipodocumento.getIdTipodocumento());
        obj.setAbreviatura(tipodocumento.getAbreviatura());
        obj.setDescripcion(tipodocumento.getDescripcion());
        tipodocumentoService.save(obj);
        } else {
        return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    @GetMapping("/list")
    public List<tipodocumento> consultarTodo() {
        return tipodocumentoService.findAll();
    }
    @GetMapping("/list/{id}")
    public tipodocumento consultaPorId(@PathVariable Integer id) {
        return tipodocumentoService.findById(id);
    }
}
