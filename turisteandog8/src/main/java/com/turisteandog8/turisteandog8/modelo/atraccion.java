/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.turisteandog8.turisteandog8.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *
 * @author nicol
 */
@Entity
@Table(name = "atraccion")
public class atraccion implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "idAtraccion")
    private Integer idAtraccion;
    @Column (name = "idCiudadAtraccion")
    private Integer idCiudadAtraccion;
    @Column (name = "descripcion")
    private String descripcion;

    public Integer getIdAtraccion() {
        return idAtraccion;
    }

    public void setIdAtraccion(Integer idAtraccion) {
        this.idAtraccion = idAtraccion;
    }

    public Integer getIdCiudadAtraccion() {
        return idCiudadAtraccion;
    }

    public void setIdCiudadAtraccion(Integer idCiudadAtraccion) {
        this.idCiudadAtraccion = idCiudadAtraccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
