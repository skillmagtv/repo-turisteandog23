package com.turisteandog8.turisteandog8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Turisteandog8Application {

	public static void main(String[] args) {
		SpringApplication.run(Turisteandog8Application.class, args);
	}

}
