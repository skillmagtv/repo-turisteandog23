/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.turisteandog8.turisteandog8.services;

import com.turisteandog8.turisteandog8.modelo.tipousuario;
import java.util.List;

/**
 *
 * @author nicol
 */
public interface tipousuarioService {
    public tipousuario save(tipousuario tipousuario); 
    public void delete(Integer id); 
    public tipousuario findById(Integer id); 
    public List<tipousuario> findAll();
}
