
function loadData(){
    let request = sendRequest('usuario/list', 'GET', '')
    let table = document.getElementById('usuarios-table');
    table.innerHTML = "";
    request.onload = function(){
        let data = request.response;
        console.log(data);
        data.forEach(element => {
            table.innerHTML += `
                <tr>
                    <th>${element.idUsuario}</th>
                    <td>${element.userName}</td>
                    <td>${element.nombre}</td>
                    <td>${element.tipoDocumento}</td>
					<td>${element.contraseña}</td>
					<td>${element.tipoUsuario}</td>
					<td>${element.correo}</td>
					<td>${element.celular}</td>
					<td>${element.PaisOrigen}</td>
					<td>${element.ciudad}</td>
					<td>${element.direccion}</td>
					<td>${element.rnt}</td>
					<td>${element.idioma}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "form_usuarios.html?id=${element.idUsuario}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteUsuario(${element.idUsuario})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos. loadData</td>
            </tr>
        `;
    }
}

function loadUsuario(idUsuario){
    let request = sendRequest('usuario/list/'+idUsuario, 'GET', '')
    let user = document.getElementById('usuario-user')
    let date = document.getElementById('usuario-date')
    let expl = document.getElementById('usuario-expl')
    let id = document.getElementById('usuario-id')
    request.onload = function(){
        
        let data = request.response
        id.value = data.idUsuario
        user.value = data.tipoUsuario
        date.value = data.fecha
        expl.value = data.descripcion
    }
    request.onerror = function(){
        alert("Error al recuperar los datos. loadUsuario");
    }
}

function deleteUsuario(idUsuario){
    let request = sendRequest('usuario/'+idUsuario, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveUsuario(){
    let user = document.getElementById('Usuario-user').value
    let date = document.getElementById('Usuario-date').value
    let expl = document.getElementById('Usuario-expl').value
    let id = document.getElementById('Usuario-id').value
    let data = {'idUsuario': id,'tipoUsuario':user,'fecha': date, 'descripcin': expl }
    let request = sendRequest('Usuario/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'usuarios.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios. saveUsuario')
    }
}