import logo from './logo.svg';
import './App.css';
import Menu from './componentes/Menu'
import {BrowserRouter, Route, Routes} from "react-router-dom"
import Inicio from './componentes/Inicio'
import Login from './componentes/Login'
import Logout from './componentes/Logout'
import Atraccion from './componentes/Atraccion';
import Departamento from './componentes/Departamento';
import CrearDepartamento from './componentes/CrearDepartamento';
import EditarDepartamento from './componentes/EditarDepartamento';
import CrearAtraccion from './componentes/CrearAtraccion';
import EditarAtraccion from './componentes/EditarAtraccion';
import Experiencia from './componentes/Experiencia';
import CrearExperiencia from './componentes/CrearExperiencia';
import EditarExperiencia from './componentes/EditarExperiencia';
import Tipousuario from './componentes/Tipousuario';
import CrearTipousuario from './componentes/CrearTipousuario'
import EditarTipousuario from './componentes/EditarTipousuario'

function App() {
 return (
    <div className="App">
     <div className='App-header'>
     <Menu/>

     <BrowserRouter>
        <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/" element={<Inicio />} />
        <Route path="/inicio" element={<Inicio />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="/Atraccion" element={<Atraccion />} />
        <Route path="/crearAtr" element={<CrearAtraccion />} />
        <Route path="/editarAtr/:id" element={<EditarAtraccion />} />
        <Route path="/experiencia" element={<Experiencia />} />
        <Route path="/crearExp" element={<CrearExperiencia />} />
        <Route path="/editarExp/:id" element={<EditarExperiencia />} />
        <Route path="/Tipousuario" element={<Tipousuario />} />
        <Route path="/CrearTipousuario" element={<CrearTipousuario />} />
        <Route path="/EditarTipousuario/:id" element={<EditarTipousuario />} />
        <Route path="/Departamento" element={<Departamento />} />
        <Route path="/crearDepartamento" element={<CrearDepartamento />} />
        <Route path="/editarDepartamento/:id" element={<EditarDepartamento />} />
        
        </Routes>
      </BrowserRouter>
     </div>
    </div>
  );
}

export default App;
