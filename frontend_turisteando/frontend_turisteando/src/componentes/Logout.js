import swal from "sweetalert";
import { useNavigate } from "react-router-dom";


const Logout = () => {
    
    sessionStorage.removeItem("cliente")
    sessionStorage.removeItem("clave")
    sessionStorage.removeItem("nombre");
    const navigate = useNavigate();
    swal("Sesión Finalizada!", "Presiona Ok!", "success");
        navigate("/");

    
    return (
        <section className="Fondo">
            <div className="Botones">
            <a href="/inicio" class="ov-btn-grow-ellipse"><strong>Volver al inicio</strong></a>
            </div>
        </section>
    );
  };
  
  export default Logout;
