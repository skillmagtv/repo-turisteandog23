import swal from "sweetalert"
import axios from "axios";
import { useState,useEffect }  from "react";
import { useNavigate} from "react-router-dom";
const URI = "http://localhost:8080/departamento/"


let headers = {
    "cliente" : sessionStorage.getItem("cliente"),
    "clave"   : sessionStorage.getItem("clave")
  };
const CrearDepartamento= () => {
    const [id_depto, setId_depto] = useState("");
    const [descripcion, setDescripcion] = useState("");
    const navigate = useNavigate();
    
    const guardar = async (e) => {
        e.preventDefault();

        const insertDepartamento= await axios({
            method: "POST",
            url: URI,
            data: {
                id_depto: id_depto, descripcion : descripcion
            },
            headers: headers 
          });
          
         
        navigate("/departamento");
      };

    const salir = () => {
        navigate("/departamento")
    }

    return(
        <div>
        <br></br>
        <h3>Crear Departamento</h3>
        <div className="container col-6" id="contenedor2">
        <form onSubmit={guardar}>
            <div className="mb-3">
            <label className="form-label">ID Departamento</label>
            <input 
                value={id_depto}
                onChange={(e) => setId_depto(e.target.value)}
                type="number"
                placeholder="Digite el Identificador" 
                className="form-control"
                required
                onInvalid={e => e.target.setCustomValidity('El campo Identificador es obligatorio')}
                onInput={e => e.target.setCustomValidity('')}

            />
            </div>
            <div className="mb-3"></div>
            <label className="form-label">Tipo de departamento</label>
            <input 
                value={descripcion}
                onChange={(e) => setDescripcion(e.target.value)}
                type="text"
                className="form-control"
                placeholder="Digite el nombre del departamento"
                required
                onInvalid={e => e.target.setCustomValidity('El campo nombre del departamento es obligatorio')}
                onInput={e => e.target.setCustomValidity('')}
            />
      
            <button type="submit" className="btn btn-warning">
            Guardar
            </button>

            <button className="btn btn-danger" type="button" onClick={salir} id="derecha">
                    Regresar
            </button>

        </form>
    </div>
    </div>
    );
};

export default CrearDepartamento;