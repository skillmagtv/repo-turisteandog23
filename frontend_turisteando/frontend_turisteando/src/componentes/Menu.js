const Menu = () => { 

    return ( 
      <nav className="navbar navbar-expand-lg">
      <div className="container-fluid">
      <div className="collapse navbar-collapse" id="navbarCollapse">
      <div className="navbar-nav">
        <ul className="navbar-nav me-auto mb-2 mb-lg-0 list-group-horizontal" >
          <li className="nav-item list-group-item" id="barra2">
            <a className="nav-link active" aria-current="page" href="/inicio">
            <strong> Inicio</strong>
            </a>
          </li>
          <li className="nav-item list-group-item" id="barra2">
            <a className="nav-link" aria-current="page" href="/experiencia">
             <strong> Experiencias</strong>
            </a>
          </li>
          <li className="nav-item list-group-item" id="barra2">
            <a className="nav-link" aria-current="page" href="/atraccion">
             <strong> Atracciones</strong>
            </a>
          </li>
          <li className="nav-item list-group-item" id="barra2">
            <a className="nav-link" aria-current="page" href="/tipousuario">
             <strong>TipoUsuario</strong>
            </a>
          </li>
          <li className="nav-item list-group-item" id="barra2">
            <a className="nav-link" aria-current="page" href="/departamento">
             <strong>Departamento</strong>
            </a>
          </li>
        </ul>
        </div>
        <div class="navbar-nav ms-auto">
                <a href="/login" className="nav-item nav-link" id="barra2"><strong>Login</strong></a>
                <a href="/logout" className="nav-item nav-link"><strong>LogOut</strong></a>
            </div>
      </div>
      </div>
   </nav>
    )
}

 
     export default Menu; 