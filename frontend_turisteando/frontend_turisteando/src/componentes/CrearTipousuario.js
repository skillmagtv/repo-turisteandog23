import swal from "sweetalert"
import axios from "axios";
import { useState,useEffect }  from "react";
import { useNavigate} from "react-router-dom";
const URI = "http://localhost:8080/tipousuario/"


let headers = {
    "cliente" : sessionStorage.getItem("cliente"),
    "clave"   : sessionStorage.getItem("clave")
  };
const CrearTipousuario = () => {
    const [id_tipo_usuario, setId_tipo_usuario] = useState("");
    const [descripcion, setDescripcion] = useState("");
    const navigate = useNavigate();
    
    const guardar = async (e) => {
        e.preventDefault();

        const insertTipousuario = await axios({
            method: "POST",
            url: URI,
            data: {
                id_tipo_usuario: id_tipo_usuario, descripcion : descripcion
            },
            headers: headers 
          });
          
         
        navigate("/tipousuario");
      };

    const salir = () => {
        navigate("/tipousuario")
    }

    return(
        <div>
        <br></br>
        <h3>Crear Tipo Usuario</h3>
        <div className="container col-6" id="contenedor2">
        <form onSubmit={guardar}>
            <div className="mb-3">
            <label className="form-label">ID Tipousuario</label>
            <input 
                value={id_tipo_usuario}
                onChange={(e) => setId_tipo_usuario(e.target.value)}
                type="number"
                placeholder="Digite el Identificador" 
                className="form-control"
                required
                onInvalid={e => e.target.setCustomValidity('El campo Identificador es obligatorio')}
                onInput={e => e.target.setCustomValidity('')}

            />
            </div>
            <div className="mb-3"></div>
            <label className="form-label">Tipo de Usuario</label>
            <input 
                value={descripcion}
                onChange={(e) => setDescripcion(e.target.value)}
                type="text"
                className="form-control"
                placeholder="Digite el nombre del tipo de usuario"
                required
                onInvalid={e => e.target.setCustomValidity('El campo nombre del tipo usuario es obligatorio')}
                onInput={e => e.target.setCustomValidity('')}
            />
      
            <button type="submit" className="btn btn-warning">
            Guardar
            </button>

            <button className="btn btn-danger" type="button" onClick={salir} id="derecha">
                    Regresar
            </button>

        </form>
    </div>
    </div>
    );
};

export default CrearTipousuario;