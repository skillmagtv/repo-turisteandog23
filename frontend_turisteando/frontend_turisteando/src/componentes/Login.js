import swal from "sweetalert";
import axios from "axios";
import { useState }  from "react";
import { useNavigate } from "react-router-dom";
const URI = "http://localhost:8080/usuario/"


const Login = () => { 
  const navigate = useNavigate();
  const [usuarios, setUsuarios] = useState([])
  const [id_usuario, setId_usuario] = useState("");
  const [contrasena, setContrasena] = useState("");
  const guardar = async (e) => {
      e.preventDefault();
      
      try {          
          const res = await axios({
              method : "GET",
              url: URI + "login?cliente="+id_usuario+"&clave="+contrasena
          });          
          setUsuarios(res.data)
          if (res.data.id_usuario==null) {
              
              swal("Usuario NO Autorizado!", "Presiona el botón!", "error");
              navigate("/");
              
          } else {
             sessionStorage.setItem("cliente",id_usuario);
             sessionStorage.setItem("clave",contrasena);
             sessionStorage.setItem("nombre",res.data.nombre);
             swal("Bienvenido "+res.data.nombre+"!", "Presiona el botón!", "success");
             navigate("/principal");
          }
      }
      catch (error) {
          swal("Operación NO realizada")
      }
    };
return ( 
      <div className="container2" >
      <div className="row" id="centrado">
          <div className="col-xs-12 col-sm-8 col-md-7 col-sm-offset-2 col-md-offset-3" >
              <form onSubmit={guardar} >
                  <fieldset >
                      <h2>Ingreso al Sistema</h2>
                      <hr className="colorgraph"/>
                      <div className="form-group">
                          <input type="text" name="id" id="id" value={id_usuario} maxLength={15} required
                          onInvalid={e => e.target.setCustomValidity('El campo Id cliente es obligatorio')}
                          onInput={e => e.target.setCustomValidity('')} class="form-control input-lg" placeholder="Digite el ID" onChange={(e) => setId_usuario(e.target.value)}/>
                      </div><br></br>
                      <div className="form-group">
                          <input type="password" name="password" id="password" 
                          value={contrasena}
                          onChange={(e) => setContrasena(e.target.value)}
                          maxLength={50}
                          required
                          onInvalid={e => e.target.setCustomValidity('El campo Contraseña  es obligatorio')}
                          onInput={e => e.target.setCustomValidity('')}
                          class="form-control input-lg" placeholder="Contraseña"/>
                      </div><br></br>
                      
                      
                      <div className="row">
                          <div className="col-xs-12 col-sm-12 col-md-12">
                              <input type="submit" className="btn  btn-success bg-warning btn-block text-dark" value="Ingresar"/>
                          </div>
                          
                      </div>
                  </fieldset>
              </form>
          </div>
      </div>
  </div>
);
}

export default Login; 