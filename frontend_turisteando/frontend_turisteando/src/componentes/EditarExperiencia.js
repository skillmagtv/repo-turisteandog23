import axios from "axios";
import { useState,useEffect }  from "react";
import { useNavigate, useParams} from "react-router-dom";
const URI = "http://localhost:8080/experiencia/"
const URI1 = "http://localhost:8080/usuario/"
const URI2 = "http://localhost:8080/atraccion/"

let headers = {
    "cliente" : sessionStorage.getItem("cliente"),
    "clave"   : sessionStorage.getItem("clave")
  };
  const EditarExperiencia = () => {
    const [id_exepriencia, setId_experiencia] = useState("");
    const [usuario, setUsuario] = useState([]);
    const [usuarios, setUsuarios] = useState([]);
    const [atraccion, setAtraccion] = useState([]);
    const [atraccions, setAtraccions] = useState([]);
    const [fecha, setFecha] = useState("");
    const [descripcion, setDescripcion] = useState("");
    const navigate = useNavigate();

    const {id} = useParams()

    const editar = async (e) => {
        e.preventDefault();
    
        const insertExperiencia = await axios({
            method: "PUT",
            url: URI,
                
            data: {
                id_experiencia:id_exepriencia,usuario: {id_usuario: usuario,nickname: null,nombre: null,apellido: null,tipo_documento: {tipo_documento: 1,abreviatura: null,descripcion: null},contrasena: null,tipo_usuario: {id_tipo_usuario: 0,descripcion: null},correo: null,celular: null,pais_origen: null,ciudad: null,direccion: null,rnt: null,idioma: null},atraccion: {id_atraccion: atraccion,ciudad: {id_ciudad: 2,departamento: {id_depto: 1,descripcion: null},descripcion: null},descripcion: null},fecha: fecha,descripcion: descripcion
            },
            headers: headers 
          });
         
        navigate("/experiencia");
      };

      useEffect( ()=>{
        getExperienciaById()
    },[])

    const getExperienciaById = async () => {

        const res =  await axios({
            method: "GET",
            url: URI+"list/"+id,
            headers: headers 
          });
        setId_experiencia(res.data.id_experiencia)
        setUsuario(res.data.usuario.id_usuario)
        setAtraccion(res.data.atraccion.id_atraccion)
        setFecha(res.data.fecha)
        setDescripcion(res.data.descripcion)
    }

    const llenarLista = async () =>{
        try {
            
            const res1 = await axios({
                method : "GET",
                url : URI1 + "list",
                headers: headers 
               
            });

            const res2 = await axios({
                method : "GET",
                url : URI2 + "list",
                headers: headers 
               
            });
            
            setUsuarios(res1.data)
            setAtraccions(res2.data)
            
        }
        catch (error) {
            //swal("No tiene Acceso a esta Opción1!", "Presiona el butón!", "error");
            //navigate("/");
        }
    }
    const salir = () => {
        navigate("/experiencia")
    }


    llenarLista();

    return(
        <div>
        <br></br>
        <h3>Editar experiencia</h3>
        <div className="container col-6" id="contenedor2">
        <form onSubmit={editar}>
            <div className="mb-3">
            <label className="form-label">ID experiencia</label>
            <input 
                value={id_exepriencia}
                onChange={(e) => setId_experiencia(e.target.value)}
                type="number"
                placeholder="Digite el Identificador" 
                className="form-control"
                required
                onInvalid={e => e.target.setCustomValidity('El campo Identificador  es obligatorio')}
                onInput={e => e.target.setCustomValidity('')}

            />

            </div>
            <div className="mb-3">
            <label className="form-label">Usuario</label>
            <select
                value={usuario}
                onChange={(e) => setUsuario(e.target.value)}
                className="form-control"
                required>
                {usuarios.map ( (usuario) => (

                    <option value={usuario.id_usuario}>{usuario.nickname}</option>
                    
                )) }
            </select>
            </div>
            <div className="mb-3">
            <label className="form-label">Atraccion</label>
            <select
                value={atraccion}
                onChange={(e) => setAtraccion(e.target.value)}
                className="form-control"
                required>
                {atraccions.map ( (atraccion) => (

                    <option value={atraccion.id_atraccion}>{atraccion.descripcion}</option>
                    
                )) }
            </select>
            </div>
            <div className="mb-3">
            <label className="form-label">Fecha de la experiencia</label>
            <input 
                value={fecha}
                onChange={(e) => setFecha(e.target.value)}
                type="text"
                className="form-control"
                placeholder="Digite la fecha de la experiencia"
                required
                onInvalid={e => e.target.setCustomValidity('La fecha de la experiencia es olbigatoria')}
                onInput={e => e.target.setCustomValidity('')}
            />
            </div>
            <div className="mb-3">
            <label className="form-label">descripcion breve de la experiencia</label>
            <input 
                value={descripcion}
                onChange={(e) => setDescripcion(e.target.value)}
                type="text"
                className="form-control"
                placeholder="Digite una descripcion muy breve de la experiencia"
                required
                onInvalid={e => e.target.setCustomValidity('La descripcion de la experiencia es obligatoria')}
                onInput={e => e.target.setCustomValidity('')}
            />
            </div>
            <button type="submit" className="btn btn-success">
            Guardar
            </button>
            <button className="btn btn-danger" type="button" onClick={salir} id="derecha">
                    Regresar
            </button>
        </form>
    </div>
    </div>
    );
};

export default EditarExperiencia;