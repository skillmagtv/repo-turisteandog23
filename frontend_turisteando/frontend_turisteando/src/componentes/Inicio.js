/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
const Inicio = () => {
    return (
<>
    <section id="pantalla-dividida" className="responsive">
        <div className="izquierda">
          <h1>Turisteando</h1>
          <h2>Contacta con Guías</h2>
          <p>Te acercamos a una gran comunidad de guías en las principales ciudades de Colombia.</p>
          <div className="link">
          </div>
          <div className="link">
            <a href="Inicio">Saber más</a>
          </div>
        </div>
        <div className="derecha"></div>
      </section>

    <section id="sec-ciudades">
        <div class="div-ciudades">
        <h4>Principales Ciudades:</h4>
         </div>
    </section>


    <section id="seccion-cards">
    <div class="cards">
        <div class="face front">
            <img src="../imagenes/bogota.jpg" alt="" className="w-100"/>
            <h3>Bogotá D.C.</h3>
        </div>
        <div class="face back">
            <h3>Bogotá D.C.</h3>
            <p>La ciudad de Bogotá es la capital de la República de Colombia y el principal destino turístico del país, pues cuenta con diversas actividades y acervos culturales, es sede de importantes universidades, y acoge eventos de nivel internacional.</p>
        </div>
    </div>

    <div class="cards">
        <div class="face front">
            <img src="imagenes/medellin.jpg" alt="Medellín" className="w-100"/>
            <h3>Medellín</h3>
        </div>
        <div class="face back">
            <h3>Medellín</h3>
            <p>Medellín es una ciudad cautivadora, no solo por su clima agradable (ronda los 19 °C durante todo el año); también por la amabilidad de su gente, su deliciosa comida y ese encanto paisa que enamora a los visitantes.</p>
        </div>
    </div>

    <div class="cards">
        <div class="face front">
            <img src="imagenes/bucaramanga.png" alt="Bucaramanga" className="w-100 h-100"/>
            <h3>Bucaramanga</h3>
        </div>
        <div class="face back">
            <h3>Bucaramanga</h3>
            <p>Bucaramanga cuenta con una muy buena infraestructura hotelera compuesta en parte por algunas de las mejores cadenas de hoteles del país. Varias zonas de la ciudad son centro de importantes restaurantes y discotecas de la ciudad.</p>
        </div>
    </div>

    <div class="cards">
        <div class="face front">
            <img src="imagenes/cali.jpg" alt="Cali" className="w-100"/>
            <h3>Cali</h3>
        </div>
        <div class="face back">
            <h3>Cali</h3>
            <p>Santiago de Cali es una de las ciudades por su gente calida, su arquitectura, monumentos historicos, lugares turísticos con valor histórico, el turismo medico que actualmente es muy fuerte en la ciudad, la recreación que ofrece para tipo de turistas, deportistas y peregrinación.</p>
        </div>
    </div>
</section>
<section>
<div class="contenedor">
        <img src="imagenes/info1.png" alt=""/>
        <p>En nuestra sección de usuarios podrás encontrar cientos de guías disponibles en las ciudades de Bogotá, Medellín, Cali y Bucaramanga.</p>
    </div>

    <div class="contenedor">
        <p>Conocer la ciudad antes de elegirla es posible, por eso en la sección de atracciones encontrarás los actractivos turisticos de cada ciudad.</p>
        <img src="imagenes/info2.png" alt=""/>
    </div>
</section>
<section id="sec-equipo">
    <div class="div-ciudades">
    <h4>Nuestro Equipo:</h4>
    </div>
    <br>
    
    </br>
</section>

<section>
<div class="row row-cols-1 row-cols-md-6 md-auto">
  <div class="col">
    <div class="card h-10 w-100 ">
      <img src="imagenes/david.jpeg" class="card-img-top" alt="..."/>
      <div class="card-body">
        <h5 class="card-title">David Rodríguez</h5>
        <p class="card-text"></p>
      </div>
      <div class="card-footer">
        <small class="text-muted">Sofware Developer</small>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card h-10 w-100">
      <img src="imagenes/nicolas.jpg" class="card-img-top" alt="..."/>
      <div class="card-body">
        <h5 class="card-title">Nicolás Murcia</h5>
        <p class="card-text"></p>
      </div>
      <div class="card-footer">
        <small class="text-muted">Sofware Developer</small>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card h-10 w-100">
      <img src="imagenes/mateo.jpg" class="card-img-top" alt="..."/>
      <div class="card-body">
        <h5 class="card-title">Mateo Zuluaga</h5>
        <p class="card-text"></p>
      </div>
      <div class="card-footer">
        <small class="text-muted">Sofware Developer</small>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card h-10 w-100" >
      <img src="imagenes/harold.jpg" class="card-img-top" alt="..."/>
      <div class="card-body">
        <h5 class="card-title">Harold Muñoz</h5>
        <p class="card-text"></p>
      </div>
      <div class="card-footer">
        <small class="text-muted">Sofware Developer</small>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card h-10 w-100">
      <img src="imagenes/yulieth.jpg" class="card-img-top" alt="..."/>
      <div class="card-body">
        <h5 class="card-title">Yulieth Vera</h5>
        <p class="card-text"></p>
      </div>
      <div class="card-footer">
        <small class="text-muted">Sofware Developer</small>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card h-10 w-100">
      <img src="imagenes/yerson.jpg" class="card-img-top" alt="..."/>
      <div class="card-body">
        <h5 class="card-title">Yerson Vargas</h5>
        <p class="card-text"></p>
      </div>
      <div class="card-footer">
        <small class="text-muted">Sofware Developer</small>
      </div>
    </div>
  </div>
</div>
</section>
<section id="sec-equipo">
    <div class="div-ciudades">
    <h4> </h4>
    </div>
    <br>
    
    </br>
</section>
<section>
<footer class="pie-pagina">
    <div class="grupo-1">
        <div class="box">
            <figure>
                <a href="#">
                    <img src="imagenes/logo nav.png" alt="Turisteando"/>
                </a>
            </figure>
        </div>
        <div class="box">
            <h2>Sobre Nosotros</h2>
            <p>Turisteando permite a los usuarios buscar guías para las principales ciudades de Colombia</p> 
            <p>C3-G08-Desarrollo de software</p>
            <p>Misión TIC 2022 - UNAB</p>
        </div>
        <div class="box1">
            <h2>Nuestras redes</h2>
            <div class="red-social">
                <a href="#" class="fa fa-facebook"></a>
                <a href="#" class="fa fa-instagram"></a>
                <a href="#" class="fa fa-twitter"></a>
                <a href="#" class="fa fa-youtube"></a>
            </div>
        </div>
    </div>
    <div class="grupo-2">
        <small>&copy; 2022 <b>Turisteando</b> - Todos los Derechos Reservados.</small>
    </div>
</footer>    
</section>    
</>
    
    
    );
  };
  
  export default Inicio;
