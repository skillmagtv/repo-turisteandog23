import swal from "sweetalert"
import axios from "axios";
import { useState,useEffect }  from "react";
import { useNavigate} from "react-router-dom";
const URI = "http://localhost:8080/atraccion/"
const URI1 = "http://localhost:8080/ciudad/"


let headers = {
    "cliente" : sessionStorage.getItem("cliente"),
    "clave"   : sessionStorage.getItem("clave")
  };
const CrearAtraccion = () => {
    const [id_atraccion, setId_atraccion] = useState("");
    const [ciudad, setCiudad] = useState([]);
    const [ciudads, setCiudads] = useState([]);
    const [descripcion, setDescripcion] = useState("");
    const navigate = useNavigate();

    
    const guardar = async (e) => {
        e.preventDefault();

        const insertAtraccion = await axios({
            method: "POST",
            url: URI,
            data: {
                id_atraccion: id_atraccion, ciudad: {id_ciudad: ciudad, id_depto : null, descripcion: null}, descripcion : descripcion
            },
            headers: headers 
          });
          
         
        navigate("/atraccion");
      };

      const llenarLista = async () =>{
        try {
            
            const res1 = await axios({
                method : "GET",
                url : URI1 + "list",
                headers: headers 
               
            });
            
            setCiudads(res1.data)
            
        }
        catch (error) {
            //swal("No tiene Acceso a esta Opción1!", "Presiona el butón!", "error");
            //navigate("/");
        }
    }

    llenarLista();

    return(
        <div>
        <br></br>
        <h3>Crear Atraccion</h3>
        <div className="container col-6" id="contenedor2">
        <form onSubmit={guardar}>
            <div className="mb-3">
            <label className="form-label">ID Atraccion</label>
            <input 
                value={id_atraccion}
                onChange={(e) => setId_atraccion(e.target.value)}
                type="number"
                placeholder="Digite el Identificador" 
                className="form-control"
                required
                onInvalid={e => e.target.setCustomValidity('El campo Identificador  es obligatorio')}
                onInput={e => e.target.setCustomValidity('')}

            />
            </div>
            <div className="mb-3">
            <label className="form-label">Ciudad</label>
            <select
                
                onChange={(e) => setCiudad(e.target.value)}
                className="form-control">
            { ciudads.map ( (ciudad) => (
                    <option value={ciudad.id_ciudad}>{ciudad.descripcion}</option>
                )) }
            </select>
            </div>
            <div className="mb-3"></div>
            <label className="form-label">Nombre de la atraccion</label>
            <input 
                value={descripcion}
                onChange={(e) => setDescripcion(e.target.value)}
                type="text"
                className="form-control"
                placeholder="Digite el nombre de la atracción"
                required
                onInvalid={e => e.target.setCustomValidity('El campo nombre de la atracción es olbigatorio')}
                onInput={e => e.target.setCustomValidity('')}
            />
      
            <button type="submit" className="btn btn-warning">
            Guardar
            </button>
           

        </form>
    </div>
    </div>
    );
};

export default CrearAtraccion;