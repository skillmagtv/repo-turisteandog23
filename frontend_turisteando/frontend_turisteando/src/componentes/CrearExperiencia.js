import swal from "sweetalert"
import axios from "axios";
import { useState,useEffect }  from "react";
import { useNavigate} from "react-router-dom";
const URI = "http://localhost:8080/experiencia/"
const URI1 = "http://localhost:8080/usuario/"
const URI2 = "http://localhost:8080/atraccion/"



let headers = {
    "cliente" : sessionStorage.getItem("cliente"),
    "clave"   : sessionStorage.getItem("clave")
  };
const CrearExperiencia = () => {
    const [id_exepriencia, setId_experiencia] = useState("");
    const [usuario, setUsuario] = useState([]);
    const [usuarios, setUsuarios] = useState([]);
    const [atraccion, setAtraccion] = useState([]);
    const [atraccions, setAtraccions] = useState([]);
    const [fecha, setFecha] = useState("");
    const [descripcion, setDescripcion] = useState("");
    const navigate = useNavigate();

    
    const guardar = async (e) => {
        e.preventDefault();

        const insertExperiencia = await axios({
            method: "POST",
            url: URI,
            data: {
                id_exepriencia: id_exepriencia, usuario: {id_usuario: usuario, nickname : null, nombre: null, apellido: null, tipo_documento: null, contrasena: null, tipo_usuario: null, correo: null, celular: null, pais_origen: null, ciudad: null, direccion: null, rnt: null, idioma: null}, atraccion: {id_atraccion: atraccion, id_ciudad: null, descripcion: null}, fecha: fecha, descripcion : descripcion
            },
            headers: headers 
          });

          navigate("/experiencia");
        };
        
        const llenarLista1 = async () =>{
            try {
                
                const res1 = await axios({
                    method : "GET",
                    url : URI1 + "list",
                    headers: headers 
                   
                });
                
                const res2 = await axios({
                    method : "GET",
                    url : URI2 + "list",
                    headers: headers 
                   
                });

                setUsuarios(res1.data)
                setAtraccions(res2.data)
                
            }
            catch (error) {
                //swal("No tiene Acceso a esta Opción1!", "Presiona el butón!", "error");
                //navigate("/");
            }
    }

    llenarLista1();

    return(
        <div>
        <br></br>
        <h3>Crear experiencia</h3>
        <div className="container col-6" id="contenedor2">
        <form onSubmit={guardar}>
            <div className="mb-3">
            <label className="form-label">ID experiencia</label>
            <input 
                value={id_exepriencia}
                onChange={(e) => setId_experiencia(e.target.value)}
                type="number"
                placeholder="Digite el Identificador" 
                className="form-control"
                required
                onInvalid={e => e.target.setCustomValidity('El campo Identificador  es obligatorio')}
                onInput={e => e.target.setCustomValidity('')}

            />
            </div>
            <div className="mb-3">
            <label className="form-label">Usuario</label>
            <select
                
                onChange={(e) => setUsuario(e.target.value)}
                className="form-control">
            { usuarios.map ( (usuario) => (
                    <option value={usuario.id_usuario}>{usuario.nickname}</option>
                )) }
            </select>
            </div>
            <div className="mb-3">
            <label className="form-label">Atraccion</label>
            <select
                
                onChange={(e) => setAtraccion(e.target.value)}
                className="form-control">
            { atraccions.map ( (atraccion) => (
                    <option value={atraccion.id_atraccion}>{atraccion.descripcion}</option>
                )) }
            </select>
            </div>
            <div className="mb-3"></div>
            <label className="form-label">Fecha de la experiencia</label>
            <input 
                value={fecha}
                onChange={(e) => setFecha(e.target.value)}
                type="text"
                className="form-control"
                placeholder="Digite la fecha de la experiencia"
                required
                onInvalid={e => e.target.setCustomValidity('El campo fecha es obligatorio')}
                onInput={e => e.target.setCustomValidity('')}
            />

            <div className="mb-3"></div>
            <label className="form-label">descripcion de la experiencia</label>
            <input 
                value={descripcion}
                onChange={(e) => setDescripcion(e.target.value)}
                type="text"
                className="form-control"
                placeholder="Digite una descripcion breve de su experiencia"
                required
                onInvalid={e => e.target.setCustomValidity('La descripcion es obligatoria')}
                onInput={e => e.target.setCustomValidity('')}
            />
      
            <button type="submit" className="btn btn-warning">
            Guardar
            </button>
           

        </form>
    </div>
    </div>
    );
};

export default CrearExperiencia;