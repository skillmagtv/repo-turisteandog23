import axios from "axios";
import { useState,useEffect }  from "react";
import { useNavigate, useParams} from "react-router-dom";
const URI = "http://localhost:8080/departamento/"

let headers = {
    "cliente" : sessionStorage.getItem("cliente"),
    "clave"   : sessionStorage.getItem("clave")
  };
  const EditarDepartamento = () => {
    const [id_depto, setId_depto] = useState("");
    const [descripcion, setDescripcion] = useState("");
    const navigate = useNavigate();

    const {id} = useParams()

    const editar = async (e) => {
        e.preventDefault();
    
        const insertDepartamento = await axios({
            method: "POST",
            url: URI,
            data: {
                id_tipo_usuario: id_depto, descripcion : descripcion
            },
            headers: headers 
          });
         
        navigate("/departamento");
      };

      useEffect( ()=>{
        getDepartamentoById()
    },[])

    const getDepartamentoById = async () => {

        const res =  await axios({
            method: "GET",
            url: URI+"list/"+id,
            headers: headers 
          });
        setId_depto(res.data.id_depto)
        setDescripcion(res.data.descripcion)
    }

    const salir = () => {
        navigate("/departamento")
    }

    return(
        <div>
        <br></br>
        <h3>Editar Departamento</h3>
        <div className="container col-6" id="contenedor2">
        <form onSubmit={editar}>
            <div className="mb-3">
            <label className="form-label">Id Departamento</label>
            <input 
                value={id_depto}
                onChange={(e) => setId_depto(e.target.value)}
                type="number"
                placeholder="Digite el Identificador" 
                className="form-control"
                required
                onInvalid={e => e.target.setCustomValidity('El campo Identificador es obligatorio')}
                onInput={e => e.target.setCustomValidity('')}

            />

            </div>
            <div className="mb-3">
            <label className="form-label">Nombre del Departamento</label>
            <input 
                value={descripcion}
                onChange={(e) => setDescripcion(e.target.value)}
                type="text"
                className="form-control"
                placeholder="Digite el nombre del Departamento"
                required
                onInvalid={e => e.target.setCustomValidity('El campo nombre del departamento es olbigatorio')}
                onInput={e => e.target.setCustomValidity('')}
            />
            </div>
            <button type="submit" className="btn btn-success">
            Guardar
            </button>
            <button className="btn btn-danger" type="button" onClick={salir} id="derecha">
                    Regresar
            </button>
        </form>
    </div>
    </div>
    );
};

export default EditarDepartamento;