import swal from "sweetalert";
import axios from "axios";
import { useState,useEffect }  from "react";
import { Link,useNavigate} from "react-router-dom";
const URI = "http://localhost:8080/tipousuario/"



let headers = {
    "cliente" : sessionStorage.getItem("cliente"),
    "clave"   : sessionStorage.getItem("clave")
  };

    const Tipousuario = () => {
    const navigate = useNavigate();
    const [tipousuarios, setTipousuarios] = useState([])
    useEffect(() =>{
        getTipousuarios()
    })

    const getTipousuarios = async () =>{
        try {
            
            const res = await axios({
                method : "GET",
                url : URI + "list",
                headers: headers 
               
            });
            
            setTipousuarios(res.data)
       
        }
        catch (error) {
            swal("No tiene Acceso a esta Opción!", "Presiona el boton!", "error");
            navigate("/");
        }
    }

const deleteTipousuario = async (id) => {
    swal(
        {
            title: "Eliminar Registro",
            text: "Está seguro de eliminar registro?",
            icons: "Warning",
            buttons: true,
            dangerMode: true,
        })
        .then(async (willDelete) =>{
            if (willDelete){
                const res = await axios({
                    method: "DELETE",
                    url: URI + id,
                    headers: headers 
                });
                swal("El resgistro se eliminó satisfactoriamente",{ 
                    icon: "success",
                });
                getTipousuarios()
            } else{
                swal("El registro no se borró")
            }
        });

}
 
const salir = () => {
    navigate("/inicio")
}
return(
        <div className='container'>
            <div className='row'>
                <div className='col'>
                    <br></br>
                    <br></br>
                    <h2>Tipo Usuario</h2>
                    <Link to="/CrearTipousuario" className='btn btn-primary mt-2 mb-2'><i className="fas fa-plus"></i></Link>
                    <table className="table table-striped table-hover">
                        <thead className="table-dark">
                            <tr>
                                <th>Id Tipo Usuario</th>
                                <th>Descripcion</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            { tipousuarios.map ( (tipousuario) => (
                                <tr key={ tipousuario.id_tipo_usuario}>
                                    <td> { tipousuario.id_tipo_usuario} </td>
                                    <td> { tipousuario.descripcion} </td>
                                    <td>
                                        <Link to={`/EditarTipousuario/${tipousuario.id_tipo_usuario}`}><i className="fa-solid fa-pen-to-square" data-toggle="tooltip" title="Editar" id="editar"></i></Link>
                                        
                                        <button id="boton1" onClick={() => deleteTipousuario(tipousuario.id_tipo_usuario)} ><i className="fa-solid fa-trash" data-toggle="tooltip" title="Eliminar" id="eliminar"></i></button>
                                    </td>
                                </tr>
                            )) }
                        </tbody>
                    </table>
                </div>    
            </div>
            <form className="d-flex">
                <button className="btn btn-primary" type="button" onClick={salir}>
                    Regresar
                </button>
            </form>
        </div>
    );
};

export default Tipousuario;