import swal from "sweetalert";
import axios from "axios";
import { useState,useEffect }  from "react";
import { Link,useNavigate} from "react-router-dom";
const URI = "http://localhost:8080/experiencia/"



let headers = {
    "cliente" : sessionStorage.getItem("cliente"),
    "clave"   : sessionStorage.getItem("clave")
  };

    const Experiencia = () => {
    const navigate = useNavigate();
    const [experiencias, setExperiencias] = useState([])
    useEffect(() =>{
        getExperiencias()
    })

    const getExperiencias = async () =>{
        try {
            
            const res = await axios({
                method : "GET",
                url : URI + "list",
                headers: headers 
               
            });
            
            setExperiencias(res.data)
       
        }
        catch (error) {
            swal("No tiene Acceso a esta Opción!", "Presiona el butón!", "error");
            navigate("/");
        }
    }

const deleteExperiencia = async (id) => {
    swal(
        {
            title: "Eliminar experiencia",
            text: "Está seguro de eliminar la experiencia?",
            icons: "Warning",
            buttons: true,
            dangerMode: true,
        })
        .then(async (willDelete) =>{
            if (willDelete){
                const res = await axios({
                    method: "DELETE",
                    url: URI + id,
                    headers: headers 
                });
                swal("La experiencia se eliminó satisfactoriamente",{ 
                    icon: "success",
                });
                getExperiencias()
            } else{
                swal("El registro no se borró")
            }
        });

}
 
const salir = () => {
    navigate("/inicio")
}
return(
        <div className='container'>
            <div className='row'>
                <div className='col'>
                    <br></br>
                    <br></br>
                    <h2>Experiencias</h2>
                    <Link to="/crearExp" className='btn btn-primary mt-2 mb-2'><i className="fas fa-plus"></i></Link>
                    <table className="table table-striped table-hover">
                        <thead className="table-dark">
                            <tr>
                                <th>Id de la Experiencia</th>
                                <th>Usuario</th>
                                <th>Atracción</th>
                                <th>Fecha</th>
                                <th>Descripcion</th>
                                <th>Acciones</th>

                            </tr>
                        </thead>
                        <tbody>
                            { experiencias.map ( (experiencia) => (
                                <tr key={ experiencia.id_experiencia}>
                                    <td> { experiencia.id_experiencia} </td>
                                    <td> { experiencia.usuario.nickname} </td>
                                    <td> { experiencia.atraccion.descripcion} </td>
                                    <td> { experiencia.fecha} </td>
                                    <td> { experiencia.descripcion} </td>
                                    <td>
                                        <Link to={`/editarExp/${experiencia.id_experiencia}`}><i className="fa-solid fa-pen-to-square" data-toggle="tooltip" title="Editar" id="editar"></i></Link>
                                        
                                        <button id="boton1" onClick={() => deleteExperiencia(experiencia.id_experiencia)} ><i className="fa-solid fa-trash" data-toggle="tooltip" title="Eliminar" id="eliminar"></i></button>
                                    </td>
                                </tr>
                            )) }
                        </tbody>
                    </table>
                </div>    
            </div>
            <form className="d-flex">
                <button className="btn btn-primary" type="button" onClick={salir}>
                    Regresar
                </button>
            </form>
        </div>
    );
};

export default Experiencia;