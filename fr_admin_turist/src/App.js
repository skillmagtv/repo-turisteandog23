import './App.css';
import Menu from './componentes/Menu';
import Departamento from './componentes/ Departamento';
import Ciudad from './componentes/Ciudad';
import Tipodocumento from './componentes/Tipodocumento';
import EditarTipodocumento from './componentes/EditarTipodocumento';
import EditarDepartamento from './componentes/EditarDepartamento';
import Login from './componentes/Login';

import {BrowserRouter, Route, Routes} from "react-router-dom"

function App() {
  return (
    <div className="App">
      <div className='App-header' >
      </div>
      <BrowserRouter>

        <Routes>
          <Route path="/menu" element={<MenuPpal />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/departamento" element={<Departamento />} />
          <Route path="/editardepartamento/:id" element={<EditarDepartamento />} />
          <Route path="/ciudad" element={<Ciudad />} />
          <Route path="/tipodocumento" element={<Tipodocumento />} />
          <Route path="/editartipodocumento/:id" element={<EditarTipodocumento />} />
        </Routes>

         <Routes>
            <Route path="/menu" element={<Menu />} />
            <Route path="/departamento" element={<Menu ruta="departamento"  />} />
            <Route path="/ciudad" element={<Menu ruta="ciudad"  />} />
            <Route path="/tipousuario" element={<Menu ruta="tipousuario"  />} />
            <Route path="/" element={<Login />} />
          </Routes>
          
      </BrowserRouter>
    </div>
  );
}
export default App;


