

import axios from "axios";
import { useState,useEffect }  from "react";
import { useNavigate, useParams} from "react-router-dom";
const URI = "http://localhost:8080/departamento/"


const EditarDepartamento = () => {
    const [departamento, setId_depto] = useState("");
    const [descripcion, setDescripcion] = useState("");
    const navigate = useNavigate();

    const {id} = useParams()

    const editar = async (e) => {
        e.preventDefault();
    
        const UpdateDepartamento = await axios({
            method: "PUT",
            url: URI,
            data: {
                tid_depto: id_depto, descripcion: descripcion
            },
            headers: headers 
          });
         
        navigate("/documentacion");
      };

      useEffect( ()=>{
        getClienteById()
    },[])

    const getClienteById = async () => {

        const res =  await axios({
            method: "GET",
            url: URI+"list/"+id,
            headers: headers 
          });
        setId_depto(res.data.id_depto)
        setDescripcion(res.data.descripcion)
    }

    return(
        <div>
        <h3>Editar Documentacion</h3>
        <div className="container col-6" id="contenedor2">
        <form onSubmit={editar}>

            <div className="mb-3">
            <label className="form-label">Departamento</label>
            <input 
                value={id_depto}
                onChange={(e) => setId_depto(e.target.value)}
                type="numeric"
                disabled="false"
                className="form-control"
            />

            </div>
            <div className="mb-3">
            <label className="form-label">Descripcion</label>
            <input
                value={descripcion}
                onChange={(e) => setDescripcion(e.target.value)}
                type="text"
                className="form-control"
                required
                onInvalid={e => e.target.setCustomValidity('El campo es obligatorio')}
                onInput={e => e.target.setCustomValidity('')}
            />
            </div>
            <button type="submit" className="btn btn-warning">
            Guardar
            </button>
        </form>
    </div>
    </div>
    );
};

export default EditarDepartamento;