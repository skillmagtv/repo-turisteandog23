import swal from "sweetalert";
import { useNavigate } from "react-router-dom";


const Logout = () => {
    
    sessionStorage.removeItem("cliente")
    sessionStorage.removeItem("clave")
    sessionStorage.removeItem("nombre");
    const navigate = useNavigate();
    swal("Sesión Finalizada!", "Presiona 'OK'", "success");
        navigate("/");

    
    return (
        <section className="Fondo">
            <div className="Botones">
            <a href="/login" class="ov-btn-grow-ellipse"><strong>Regresar al Login</strong></a>
            </div>
        </section>
    );
  };
  
  export default Logout;
