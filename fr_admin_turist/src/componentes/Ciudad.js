import swal from "sweetalert";
import axios from "axios";
import { useState,useEffect }  from "react";
import { Link,useNavigate} from "react-router-dom";
const URI = "http://localhost:8080/ciudad/"



    const Ciudad = (props) => {
    let headers=props.headers    
    const navigate = useNavigate();
    const [ciudads, setCiudads] = useState([])
    useEffect(() =>{
        getCiudads()
    })

    const getCiudads = async () =>{
        try {
            
            const res = await axios({
                method : "GET",
                url : URI + "list",
                headers: headers 
               
            });
            
            setCiudads(res.data)
       
        }
        catch (error) {
            swal("No tiene Acceso a esta Opción!", "Presiona el butón!", "error");
            navigate("/");
        }
    }

const deleteCiudad = async (id) => {
    swal(
        {
            title: "Eliminar Registro",
            text: "Está seguro de eliminar registro?",
            icons: "Warning",
            buttons: true,
            dangerMode: true,
        })
        .then(async (willDelete) =>{
            if (willDelete){
                const res = await axios({
                    method: "DELETE",
                    url: URI + id,
                    headers: headers 
                });
                swal("El resgistro se eliminó satisfactoriamente",{ 
                    icon: "success",
                });
                getCiudads()
            } else{
                swal("El registro no se borró")
            }
        });

}
 
const salir = () => {
    navigate("/inicio")
}
return(
        <div className='container'>
            <div className='row'>
                <div className='col'>
                    <br></br>
                    <br></br>
                    <h2>Ciudades</h2>
                    <Link to="/crearAtr" className='btn btn-primary mt-2 mb-2'><i className="fas fa-plus"></i></Link>
                    <table className="table table-striped table-hover">
                        <thead className="table-dark">
                            <tr>
                                <th>Id de la ciudad</th>
                                <th>Departamento</th>
                                <th>Nombre de la ciudad</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            { ciudads.map ( (ciudad) => (
                                <tr key={ ciudad.id_ciudad}>
                                    <td> { ciudad.id_ciudad} </td>
                                    <td> { ciudad.departamento.descripcion} </td>
                                    <td> { ciudad.descripcion} </td>
                                    <td>
                                        <Link to={`/editarCiudad/${ciudad.id_ciudad}`}><i className="fa-solid fa-pen-to-square" data-toggle="tooltip" title="Editar" id="editar"></i></Link>
                                        
                                        <button id="boton1" onClick={() => deleteCiudad(ciudad.id_ciudad)} ><i className="fa-solid fa-trash" data-toggle="tooltip" title="Eliminar" id="eliminar"></i></button>
                                    </td>
                                </tr>
                            )) }
                        </tbody>
                    </table>
                </div>    
            </div>
            <form className="d-flex">
                <button className="btn btn-primary" type="button" onClick={salir}>
                    Regresar
                </button>
            </form>
        </div>
    );
};

export default Ciudad;