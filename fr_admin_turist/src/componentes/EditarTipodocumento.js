
import axios from "axios";
import { useState,useEffect }  from "react";
import { useNavigate, useParams} from "react-router-dom";
const URI = "http://localhost:8080/tipodocumento/"

let headers = {
    "usuario" : sessionStorage.getItem("cliente"),
    "clave"   : sessionStorage.getItem("clave")
  };
const EditarTipodocumento = () => {
    const [tipo_documento, setTipo_documento] = useState("");
    const [abreviatura, setAbreviatura] = useState("");
    const [descripcion, setDescripcion] = useState("");
    const navigate = useNavigate();

    const {id} = useParams()

    const editar = async (e) => {
        e.preventDefault();
    
        const UpdateTipodocumento = await axios({
            method: "PUT",
            url: URI,
            data: {
                tipo_documento: tipo_documento, abreviatura: abreviatura, descripcion: descripcion
            },
            headers: headers 
          });
         
        navigate("/documentacion");
      };

      useEffect( ()=>{
        getClienteById()
    },[])

    const getClienteById = async () => {

        const res =  await axios({
            method: "GET",
            url: URI+"list/"+id,
            headers: headers 
          });
        setTipo_documento(res.data.tipo_documento)
        setAbreviatura(res.data.abreviatura)
        setDescripcion(res.data.descripcion)
    }

    return(
        <div>
        <h3>Editar Documentación</h3>
        <div className="container col-6" id="contenedor2">
        <form onSubmit={editar}>
            <div className="mb-3">
            <label className="form-label">Tipo de Documento</label>
            <input 
                value={tipo_documento}
                onChange={(e) => setTipo_documento(e.target.value)}
                type="numeric"
                disabled="false"
                className="form-control"
            />
            </div>
            <div className="mb-3">
            <label className="form-label">Abreviatura</label>
            <input 
                value={abreviatura}
                onChange={(e) => setAbreviatura(e.target.value)}
                type="text"
                className="form-control"
                required
                onInvalid={e => e.target.setCustomValidity('El campo es obligatorio')}
                onInput={e => e.target.setCustomValidity('')}
            />
            </div>
            <div className="mb-3">
            <label className="form-label">Descripcion</label>
            <input
                value={descripcion}
                onChange={(e) => setDescripcion(e.target.value)}
                type="text"
                className="form-control"
                required
                onInvalid={e => e.target.setCustomValidity('El campo es obligatorio')}
                onInput={e => e.target.setCustomValidity('')}
            />
            </div>
            <button type="submit" className="btn btn-warning">
            Guardar
            </button>
        </form>
    </div>
    </div>
    );
};

export default EditarTipodocumento;