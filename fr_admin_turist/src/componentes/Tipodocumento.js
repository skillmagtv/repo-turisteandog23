import swal from "sweetalert";
import axios from "axios";
import { useState,useEffect }  from "react";
import { Link,useNavigate} from "react-router-dom";
const URI = "http://localhost:8080/tipodocumento/"



let headers = {
    "cliente" : sessionStorage.getItem("cliente"),
    "clave"   : sessionStorage.getItem("clave")
  };

    const Tipodocumento = () => {
    const navigate = useNavigate();
    const [documentos, setdocumentos] = useState([])
    useEffect(() =>{
        getdocumentos()
    })

    const getdocumentos = async () =>{
        try {
            
            const res = await axios({
                method : "GET",
                url : URI + "list",
                headers: headers 
               
            });
            
            setdocumentos(res.data)
       
        }
        catch (error) {
            swal("No tiene Acceso a esta Opción!", "Presiona el botón!", "error");
            navigate("/tipodocumento");
        }
    }

const deleteTipodocumento = async (id) => {
    swal(
        {
            title: "Eliminar Registro",
            text: "Está seguro de eliminar registro?",
            icons: "Warning",
            buttons: true,
            dangerMode: true,
        })
        .then(async (willDelete) =>{
            if (willDelete){
                const res = await axios({
                    method: "DELETE",
                    url: URI + id,
                    headers: headers 
                });
                swal("El resgistro se eliminó satisfactoriamente",{ 
                    icon: "success",
                });
                getdocumentos()
            } else{
                swal("El registro no se borró")
            }
        });

}
 
const salir = () => {
    navigate("/inicio")
}
return(
        <div className='container'>
            <div className='row'>
                <div className='col'>
                    <br></br>
                    <br></br>
                    <h2>Tipo Documento</h2>
                    <Link to="/crearAtr" className='btn btn-primary mt-2 mb-2'><i className="fas fa-plus"></i></Link>
                    <table className="table table-striped table-hover">
                        <thead className="table-dark">
                            <tr>
                                <th>Tipo de Documento</th>
                                <th>Abreviatura</th>
                                <th>Descripcion</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            { documentos.map ( (tipodocumento) => (
                                <tr key={ tipodocumento.tipo_documento}>
                                    <td> { tipodocumento.tipo_documento} </td>
                                    <td> { tipodocumento.abreviatura} </td>
                                    <td> { tipodocumento.descripcion} </td>
                                    <td>
                                        <Link to={`/editarTipodocumento/${tipodocumento.tipo_documento}`}><i className="fa-solid fa-pen-to-square" data-toggle="tooltip" title="Editar" id="editar">Editar</i></Link>
                                        <button id="boton1" onClick={() => deleteTipodocumento(tipodocumento.tipo_documento)} ><i className="fa-solid fa-pen-to-square"  title="Eliminar" id="eliminar">Eliminar</i></button>
                                    </td>
                                </tr>
                            )) }
                        </tbody>
                    </table>
                </div>    
            </div>
            <form className="d-flex">
                <button className="btn btn-warning" type="button" onClick={salir}>
                    Regresar
                </button>
            </form>
        </div>
    );
};

export default Tipodocumento;