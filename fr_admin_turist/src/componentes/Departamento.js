import swal from "sweetalert";
import axios from "axios";
import { useState,useEffect }  from "react";
import { Link,useNavigate} from "react-router-dom";
const URI = "http://localhost:8080/departamento/"



    const Departamento = (props) => {
    let headers=props.headers    
    const navigate = useNavigate();
    const [departamentos, setDepartamentos] = useState([])
    useEffect(() =>{
        getDepartamentos()
    })

    const getDepartamentos = async () =>{
        try {
            
            const res = await axios({
                method : "GET",
                url : URI + "list",
                headers: headers 
               
            });
            
            setDepartamentos(res.data)
       
        }
        catch (error) {
            swal("No tiene Acceso !", "Presionar el botón!", "error");
            navigate("/");
        }
    }

const deleteDepartamento = async (id) => {
    swal(
        {
            title: "Eliminar Registro",
            text: "Está seguro de eliminar ?",
            icons: "Warning",
            buttons: true,
            dangerMode: true,
        })
        .then(async (willDelete) =>{
            if (willDelete){
                const res = await axios({
                    method: "DELETE",
                    url: URI + id,
                    headers: headers 
                });
                swal("El resgistro se eliminó correctamente",{ 
                    icon: "success",
                });
                getDepartamentos()
            } else{
                swal("El registro no se borró")
            }
        });

}
 
const salir = () => {
    navigate("/inicio")
}
return(
        <div className='container'>
            <div className='row'>
                <div className='col'>
                    <br></br>
                    <br></br>
                    <h2>Departamentos</h2>
                    <Link to="/crearAtr" className='btn btn-primary mt-2 mb-2'><i className="fas fa-plus"></i></Link>
                    <table className="table table-striped table-hover">
                        <thead className="table-dark">
                        <tr>
                                <th>Id Departamento</th>
                                <th>Descripcion</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            { departamentos.map ( (departamento) => (
                                <tr key={ departamento.id_depto}>
                                    <td> { departamento.id_depto} </td>
                                    <td> { departamento.descripcion} </td>
                                    <td>
                                        <Link to={`/editarC/${departamento.id_depto}`}><i className="fa-solid fa-pen-to-square" data-toggle="tooltip" title="Editar" id="editar"></i></Link>
                                        
                                        <button id="boton1" onClick={() => deleteDepartamento(departamento.id_depto)} ><i className="fa-solid fa-trash" data-toggle="tooltip" title="Eliminar" id="eliminar"></i></button>
                                    </td>
                                </tr>
                            )) }
                        </tbody>
                    </table>
                </div>    
            </div>
            <form className="d-flex">
                <button className="btn btn-primary" type="button" onClick={salir}>
                    Regresar
                </button>
            </form>
        </div>
    );
};

export default Departamento;